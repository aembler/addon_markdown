module.exports = function(grunt) {
	var themePath = '../web/packages/ae2014/themes/ae2014';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		less: {
			production: {
				options: {
					yuicompress: true,
				},
				files: {
					'../packages/markdown/css/bootstrap-markdown-editor.css': 'bootstrap-markdown-editor.less'
				}
			}
		},
		uglify: {
			production: {
				files: [{
					expand: true,
					src: 'bootstrap-markdown-editor.js',
					dest: '../packages/markdown/js/'
				}]
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.registerTask('default', ['less:production','uglify:production']);
};